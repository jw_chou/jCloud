package com.hxd.core.config;

import java.util.ArrayList;
import java.util.List;

public class AuthConfig {

	public static String TARGET = "/**";
	public static String REPLACEMENT = "";
	public static String AUTH_KEY = "token";
	private static List<String> defaultSkipUrl = new ArrayList<>();

	static {
		defaultSkipUrl.add("/login");
		defaultSkipUrl.add("/logout");
		defaultSkipUrl.add("/oauth/token");
		defaultSkipUrl.add("/oauth/user");
	}

	/**
	 * 默认无需鉴权的API
	 */
	public static List<String> getDefaultSkipUrl() {
		return defaultSkipUrl;
	}
}
