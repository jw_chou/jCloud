package com.hxd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import de.codecentric.boot.admin.server.config.EnableAdminServer;

@EnableAdminServer
@SpringBootApplication
public class OpsServeRun {

	public static void main(String[] args) {
		SpringApplication.run(OpsServeRun.class, args);
	}
}
