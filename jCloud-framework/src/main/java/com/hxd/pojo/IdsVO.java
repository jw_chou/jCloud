package com.hxd.pojo;

import java.util.List;

import lombok.Data;

@Data
public class IdsVO {

	private List<Long> ids;
	
}
