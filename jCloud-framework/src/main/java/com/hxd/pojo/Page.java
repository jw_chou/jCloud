package com.hxd.pojo;

import java.util.Map;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class Page<T> extends com.baomidou.mybatisplus.extension.plugins.pagination.Page<T> {
	
	private static final long serialVersionUID = 1L;
	
	private static long pageNum = 1;
	private static long pageSize  = 5;

	public Page(long current, long size) {
		super(current, size);
	}
	
	public Page(Map<String, Object> params) {
		super(params.get("pageNum") != null ? Long.parseLong(params.get("pageNum") + ""): pageNum,
				params.get("pageSize") != null ? Long.parseLong(params.get("pageSize") + "") : pageSize);
    }
}
