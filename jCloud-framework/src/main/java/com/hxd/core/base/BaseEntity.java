package com.hxd.core.base;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;

@Data
@FieldNameConstants
@Accessors(chain = true)
public class BaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "主键id")
	protected Long id;
	
	@JsonIgnore
	@TableField(exist = false)
	@ApiModelProperty(value = "页数")
	private long pageNum;
	
	@JsonIgnore
	@TableField(exist = false)
	@ApiModelProperty(value = "每页记录数")
	private long pageSize;
	
}
