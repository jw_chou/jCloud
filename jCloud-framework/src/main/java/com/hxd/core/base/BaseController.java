package com.hxd.core.base;

import static com.hxd.Result.success;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import io.swagger.annotations.ApiOperation;

import com.hxd.pojo.IdsVO;
import com.hxd.pojo.Page;
import com.hxd.Result;

/**
 * 基础控制器
 * @author zhoujw
 *
 * @param <S> Service
 * @param <T> Bean
 */
public class BaseController<S extends BaseService<T>, T extends BaseEntity> {
	
	@Autowired
	protected S service;
	
	@GetMapping
	@ApiOperation(value = "查询列表")
	public Result<Page<T>> list(Map<String, Object> params) {
		return success(service.page(params));
	}
	
	@GetMapping("{id}")
	@ApiOperation(value = "查询详情")
	public Result<T> getById(@PathVariable("id") Long id) {
		return success(service.getById(id));
	}
	
	@DeleteMapping
	@ApiOperation(value = "批量删除数据")
	public Result<Boolean> delete(@RequestBody IdsVO vo) {
		return success(service.removeByIds(vo.getIds()));
	}
	
}
