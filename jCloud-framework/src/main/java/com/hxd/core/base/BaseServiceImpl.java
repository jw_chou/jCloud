package com.hxd.core.base;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hxd.core.config.mybatis.QWrapper;
import com.hxd.pojo.Page;

import lombok.extern.slf4j.Slf4j;

/**
 * 基础实现
 * @author zhoujw
 */
@Slf4j
public class BaseServiceImpl<M, T extends BaseEntity> extends ServiceImpl<BaseMapper<T>, T> implements BaseService<T> {
	
	@Autowired
	protected M mapper;
	
	@Override
	public Page<T> page(Map<String, Object> params) {
		QWrapper<T> wrapper = new QWrapper<T>();
		Object value = null;
		for (String key : params.keySet()) {
			value = params.get(key);
			if(value instanceof String) {
				wrapper.like(key, value);
			}else if(value instanceof Integer) {
				wrapper.eq(key, value);
			}
		}
		Page<T> page;
		try {
			page = page(new Page<T>(params), wrapper);
		} catch (Exception e) {
			log.error(e.getMessage());
			page = page(new Page<T>(params));
		}
		return page;
	}

}
