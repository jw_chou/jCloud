package com.hxd.core.base;

import java.util.Map;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hxd.pojo.Page;
import com.hxd.utils.IdWorker;

import cn.hutool.core.bean.copier.CopyOptions;

/**
 * IService接口扩展
 * @author zhoujw
 */
public interface BaseService<T extends BaseEntity> extends IService<T> {
	
	IdWorker IW = new IdWorker(1);
	
	/**
	 * hutool copyProperties 过滤null 不复制
	 */
	CopyOptions IGNORE_NULL = CopyOptions.create().setIgnoreNullValue(true).setIgnoreError(true);
	
	Page<T> page(Map<String, Object> params);
	
}
