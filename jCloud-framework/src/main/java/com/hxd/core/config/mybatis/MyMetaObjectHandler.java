package com.hxd.core.config.mybatis;

import java.util.Date;

import org.apache.ibatis.reflection.MetaObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;

/**
 * 填充器
 */
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(MyMetaObjectHandler.class);
	
	@Override
	public void insertFill(MetaObject metaObject) {
		LOGGER.info("start insert fill ....");
		this.setFieldValByName("createDate", new Date().getTime(), metaObject);
	}

	@Override
	public void updateFill(MetaObject metaObject) {
		LOGGER.info("start update fill ....");
        this.setFieldValByName("updateDate", new Date().getTime(), metaObject);
	}

}
