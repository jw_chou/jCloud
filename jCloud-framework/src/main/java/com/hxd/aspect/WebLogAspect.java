package com.hxd.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import cn.hutool.json.JSONUtil;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

@Aspect
@Component
public class WebLogAspect {

    private static final Logger logger = LoggerFactory.getLogger(WebLogAspect.class);

    @Pointcut("execution(public * com.hxd.controller.*.*(..)) || execution(public * com.hxd.base.controller.*.*(..))")
    public void webLog() {
    }

    @Around("webLog()")
    public Object  doAround(ProceedingJoinPoint pjp) throws Throwable {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        
        logger.info("URL : " + request.getRequestURL().toString());
        logger.info("HTTP_METHOD : " + request.getMethod());
        logger.info("IP : " + request.getRemoteAddr());
        logger.info("BODY :	" +JSONUtil.toJsonStr( pjp.getArgs()));
        Enumeration<String> enu = request.getParameterNames();
        String urlParameters = null;
        while (enu.hasMoreElements()) {
            String name = (String) enu.nextElement();
            if(urlParameters==null) {
            	urlParameters = name+ "="+request.getParameter(name);
            }else {
            	urlParameters = urlParameters + "," + name+ "="+request.getParameter(name);;
            }
        }
        logger.info("URL_PARAMETERS : " + urlParameters);
        
        Enumeration<String> headers = request.getParameterNames();
        String headersStr = null;
        while (headers.hasMoreElements()) {
            String name = (String) headers.nextElement();
            if(headersStr==null) {
            	headersStr = name+ "="+request.getParameter(name);
            }else {
            	headersStr = headersStr + "," + name+ "="+request.getParameter(name);;
            }
        }
        logger.info("HEADRES: " + headersStr);
        
        return pjp.proceed();
    }

    @AfterReturning(returning = "ret", pointcut = "webLog()")
    public void doAfterReturning(Object ret) throws Throwable {
    	if(StringUtils.isEmpty(ret)){
    		 logger.info("RESPONSE : 返回ret 为空");
    	}else{
    		String res = JSONUtil.toJsonStr(ret);
    		if(res.length()<=10000) {
    			logger.info("RESPONSE : " + res);
    		}else {
    			logger.info("RESPONSE IS TO LONG");
    		}
    	}
    }
}
