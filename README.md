```
spring cloud alibaba
|
├──common        通用
├──framework     框架
├──gateway       网关  demo 端口8000
├──auth oauth2.0 授权  demo 端口8100
├──service       服务
   ├──system     系统  demo 端口8080
   ├──api        接口  demo 端口8085
├──ops           运维  demo 端口9000
```