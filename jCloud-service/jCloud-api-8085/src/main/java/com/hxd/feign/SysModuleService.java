package com.hxd.feign;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import com.hxd.feign.factory.SysModuleFallbackFactory;
import com.hxd.pojo.SysModuleVO;
import com.hxd.constant.FeignClientConstants;

@FeignClient(name = FeignClientConstants.SYSTEM, fallbackFactory = SysModuleFallbackFactory.class)
public interface SysModuleService {
	
	@GetMapping("sys-module/tree")
	List<SysModuleVO> getTree();

}
