package com.hxd.mapper;

import com.hxd.entity.SysModule;
import com.hxd.pojo.SysModuleVO;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统基础 - 模块信息 Mapper 接口
 * </p>
 *
 * @author zhoujw
 * @since 2020-04-22
 */
public interface SysModuleMapper extends BaseMapper<SysModule> {

	List<SysModuleVO> selectList();
	
}
