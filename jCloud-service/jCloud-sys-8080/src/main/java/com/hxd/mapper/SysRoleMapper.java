package com.hxd.mapper;

import com.hxd.entity.SysRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统基础 - 角色表 Mapper 接口
 * </p>
 *
 * @author zhoujw
 * @since 2020-04-23
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

}
