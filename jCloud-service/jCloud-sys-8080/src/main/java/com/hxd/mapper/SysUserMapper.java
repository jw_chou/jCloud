package com.hxd.mapper;

import com.hxd.entity.SysUser;
import com.hxd.pojo.SysUserVO;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统基础 - 用户表 Mapper 接口
 * </p>
 *
 * @author zhoujw
 * @since 2020-04-22
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

	List<SysUserVO> selectList(Map<String, Object> params);
	
}
