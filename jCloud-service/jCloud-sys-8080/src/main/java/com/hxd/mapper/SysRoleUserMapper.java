package com.hxd.mapper;

import com.hxd.entity.SysRoleUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统基础 - 用户角色表 Mapper 接口
 * </p>
 *
 * @author zhoujw
 * @since 2020-04-28
 */
public interface SysRoleUserMapper extends BaseMapper<SysRoleUser> {

}
