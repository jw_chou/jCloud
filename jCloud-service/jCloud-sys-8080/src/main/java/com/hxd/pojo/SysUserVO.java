package com.hxd.pojo;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value="sys_user模型", description="系统基础 - 用户表")
public class SysUserVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "主键ID")
	private Long id;
	
    @ApiModelProperty(value = "用户编码")
    private String code;
    
    @ApiModelProperty(value = "用户昵称")
    private String name;
    
    @ApiModelProperty(value = "用户类型（0：普通用户；1：超级管理员；）")
    private Integer type;
    
    @ApiModelProperty(value = "用户状态（0：正常；1：停用；）")
    private Integer state;
    
    @ApiModelProperty(value = "所属组织id")
    private Long orgId;
    
    @ApiModelProperty(value = "所属组织名称")
    private String orgName;
    
    @ApiModelProperty(value = "性别")
    private Integer gender;
    
    @ApiModelProperty(value = "身份号码")
    private String idCard;
    
    @ApiModelProperty(value = "出生日期")
    private Long birthDate;
    
    @ApiModelProperty(value = "职位信息")
    private String positions;
    
    @ApiModelProperty(value = "是否领导")
    private Integer isLeader;
    
    @ApiModelProperty(value = "移动电话")
    private String phone;
    
    @ApiModelProperty(value = "座机号码")
    private String telephone;
    
    @ApiModelProperty(value = "电子邮箱")
    private String email;
    
    @ApiModelProperty(value = "家庭住址")
    private String address;
    
    @ApiModelProperty(value = "个人头像")
    private String headImage;
    
    @ApiModelProperty(value = "用户签名")
    private String signImage;
    
    @ApiModelProperty(value = "个性配置（JSON）")
    private String configure;
    
    @ApiModelProperty(value = "设备类型")
    private String devicePlatform;
    
    @ApiModelProperty(value = "设备号")
    private String deviceUid;
    
    @ApiModelProperty(value = "排序编码")
    private Integer sort;
    
    @ApiModelProperty(value = "备注说明")
    private String remark;
    
    @ApiModelProperty(value = "账号/工号")
    private String identifier;
    
    @ApiModelProperty(value = "角色ID")
    private Long roleId;
    
    @ApiModelProperty(value = "角色名称")
    private String roleName;
}