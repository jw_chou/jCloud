package com.hxd.pojo;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value="sys_module_function模型", description="系统基础 - 模块功能表")
public class SysModuleFunctionVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "主键ID")
	private Long id;
	
    @ApiModelProperty(value = "模块编码")
    private String moduleCode;
    
    @ApiModelProperty(value = "功能分组")
    private String groups;
    
    @ApiModelProperty(value = "功能名称")
    private String name;
    
    @ApiModelProperty(value = "是否启用（0：否；1：是）")
    private Integer isEnable;
    
    @ApiModelProperty(value = "排序编码")
    private Integer sort;
    
    @ApiModelProperty(value = "权限Url")
    private String permissionUrl;
    
    @ApiModelProperty(value = "权限Key")
    private String permission;
    
}