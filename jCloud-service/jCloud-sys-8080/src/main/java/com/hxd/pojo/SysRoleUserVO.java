package com.hxd.pojo;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value="sys_role_user模型", description="系统基础 - 用户角色表")
public class SysRoleUserVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "主键ID")
	private Long id;
	
    @ApiModelProperty(value = "角色id")
    private Long roleId;
    
    @ApiModelProperty(value = "用户id")
    private Long userId;
    
}