package com.hxd.pojo;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value="sys_module模型", description="系统基础 - 模块信息")
public class SysModuleVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "主键ID")
	private Long id;
	
    @ApiModelProperty(value = "模块编码")
    private String code;
    
    @ApiModelProperty(value = "模块名称")
    private String name;
    
    @ApiModelProperty(value = "模块状态（0：正常；1：停用）")
    private Integer state;
    
    @ApiModelProperty(value = "上级id")
    private Long parentId;
    
    @ApiModelProperty(value = "权限控制（0：否；1：是）")
    private Integer isRole;
    
    @ApiModelProperty(value = "链接地址")
    private String link;
    
    @ApiModelProperty(value = "显示图标")
    private String icon;
    
    @ApiModelProperty(value = "排序编码")
    private Integer sort;
    
    @ApiModelProperty(value = "备注说明")
    private String remark;
    
    @ApiModelProperty(value = "子节点")
    private List<SysModuleVO> children;
    
    @ApiModelProperty(value = "功能权限")
    private List<SysModuleFunctionVO> function;
    
}