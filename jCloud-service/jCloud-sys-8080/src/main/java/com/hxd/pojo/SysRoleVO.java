package com.hxd.pojo;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value="sys_role模型", description="系统基础 - 角色表")
public class SysRoleVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "主键ID")
	private Long id;
	
    @ApiModelProperty(value = "角色类型（0：普通角色；1：系统设定）")
    private Integer category;
    
    @ApiModelProperty(value = "角色编码")
    private String code;
    
    @ApiModelProperty(value = "角色名称")
    private String name;
    
    @ApiModelProperty(value = "排序编码")
    private Integer sort;
    
    @ApiModelProperty(value = "备注说明")
    private String remark;
    
    @ApiModelProperty(value = "是否删除（0：否；1：是）")
    private Integer isDel;
    
    @ApiModelProperty(value = "创建时间")
    private Long createDate;
    
    @ApiModelProperty(value = "创建人")
    private Long createUserId;
    
    @ApiModelProperty(value = "创建人名称")
    private String createUserName;
    
    @ApiModelProperty(value = "更新时间")
    private Long updateDate;
    
    @ApiModelProperty(value = "更新人")
    private Long updateUserId;
    
    @ApiModelProperty(value = "更新人名称")
    private String updateUserName;
    
}