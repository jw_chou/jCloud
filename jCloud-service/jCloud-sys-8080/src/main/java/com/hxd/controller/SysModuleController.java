package com.hxd.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.hxd.Result;
import com.hxd.pojo.SysModuleVO;
import com.hxd.service.SysModuleService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>
    * 系统基础 - 模块信息 前端控制器
    * </p>
 *
 * @author zhoujw
 * @since 2020-04-22
 * @version v1.0
 */
@Slf4j
@Api(tags="系统基础 - 模块信息")
@RestController
@RequestMapping("/sys-module")
public class SysModuleController {
	
	@Autowired
	private SysModuleService service;
	
	@GetMapping("/tree")
	@ApiOperation(value = "查询列表-树结构")
	public Result<List<SysModuleVO>> tree() {
		List<SysModuleVO> list = service.getTree();
		log.debug("日志测试数据：" + JSONObject.toJSONString(list));
		log.error("hello world");
		return Result.success(list);
	}
}
