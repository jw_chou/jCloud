package com.hxd.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hxd.Result;
import com.hxd.core.base.BaseController;
import com.hxd.entity.SysRole;
import com.hxd.pojo.SysRoleVO;
import com.hxd.service.SysRoleService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * <p>
    * 系统基础 - 角色表 前端控制器
    * </p>
 *
 * @author zhoujw
 * @since 2020-04-23
 * @version v1.0
 */
@Api(tags="系统基础 - 角色表")
@RestController
@RequestMapping("/sys-role")
public class SysRoleController extends BaseController<SysRoleService, SysRole> {

    @PostMapping
    @ApiOperation(value = "新增数据")
    public Result<SysRoleVO> save(@RequestBody SysRoleVO vo) {
		return Result.success(service.save(vo));
    }
    
    @PutMapping
    @ApiOperation(value = "更新数据")
    public Result<SysRoleVO> update(@RequestBody SysRoleVO vo) {
		return Result.success(service.update(vo));
    }
}
