package com.hxd.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hxd.Result;
import com.hxd.core.base.BaseController;
import com.hxd.entity.SysUser;
import com.hxd.pojo.SysUserVO;
import com.hxd.service.SysUserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * <p>
    * 系统基础 - 用户表 前端控制器
    * </p>
 *
 * @author zhoujw
 * @since 2020-04-22
 * @version v1.0
 */
@Api(tags="系统基础 - 用户表")
@RestController
@RequestMapping("/sys-user")
public class SysUserController extends BaseController<SysUserService, SysUser> {

    @PostMapping
    @ApiOperation(value = "新增数据")
    public Result<SysUserVO> save(@RequestBody SysUserVO vo) {
		return Result.success(service.save(vo));
    }
    
    @PutMapping
    @ApiOperation(value = "更新数据")
    public Result<SysUserVO> update(@RequestBody SysUserVO vo) {
		return Result.success(service.update(vo));
    }
    
}
