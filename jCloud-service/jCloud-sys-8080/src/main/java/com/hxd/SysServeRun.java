package com.hxd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class SysServeRun {

	public static void main(String[] args) {
		System.setProperty("nacos.logging.default.config.enabled", "false");
        SpringApplication.run(SysServeRun.class, args);
    }
}
