package com.hxd.service;

import com.hxd.core.base.BaseService;
import com.hxd.entity.SysRole;
import com.hxd.pojo.SysRoleVO;

/**
 * <p>
    * 系统基础 - 角色表 服务类
    * </p>
 *
 * @author zhoujw
 * @since 2020-04-23
 * @version v1.0
 */
public interface SysRoleService extends BaseService<SysRole> {
	
	SysRoleVO save(SysRoleVO vo);
	
	SysRoleVO update(SysRoleVO vo);
}
