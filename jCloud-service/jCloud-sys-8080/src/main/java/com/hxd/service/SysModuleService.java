package com.hxd.service;

import java.util.List;

import com.hxd.core.base.BaseService;
import com.hxd.entity.SysModule;
import com.hxd.pojo.SysModuleVO;

/**
 * <p>
    * 系统基础 - 模块信息 服务类
    * </p>
 *
 * @author zhoujw
 * @since 2020-04-22
 * @version v1.0
 */
public interface SysModuleService extends BaseService<SysModule> {
	
	/**
	 * 自定义树-模块对应权限
	 */
	List<SysModuleVO> getTree();
}
