package com.hxd.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hxd.core.base.BaseServiceImpl;
import com.hxd.core.config.mybatis.QWrapper;
import com.hxd.entity.SysRoleUser;
import com.hxd.mapper.SysRoleUserMapper;
import com.hxd.pojo.SysRoleUserVO;
import com.hxd.service.SysRoleUserService;

import cn.hutool.core.util.ObjectUtil;

/**
 * <p>
    * 系统基础 - 用户角色表 服务实现类
    * </p>
 *
 * @author zhoujw
 * @since 2020-04-28
 * @version v1.0
 */
@Service
public class SysRoleUserServiceImpl extends BaseServiceImpl<SysRoleUserMapper, SysRoleUser> implements SysRoleUserService {

	@Override
	@Transactional
	public Boolean saveOrUpdate(SysRoleUserVO vo) {
		if(vo.getUserId() != 0) {
			SysRoleUser entity = getOne(new QWrapper<SysRoleUser>().eq(SysRoleUser.Fields.userId, vo.getUserId()));
			if(ObjectUtil.isNull(entity)) {
				entity = new SysRoleUser();
				entity.setId(IW.nextId());
			}
			entity.setRoleId(vo.getRoleId());
			if(!saveOrUpdate(entity)) {
				return false;
			}
		}
		return true;
	}
}
