package com.hxd.service;

import java.util.Map;

import com.hxd.core.base.BaseService;
import com.hxd.entity.SysUser;
import com.hxd.pojo.Page;
import com.hxd.pojo.SysUserVO;

/**
 * <p>
    * 系统基础 - 用户表 服务类
    * </p>
 *
 * @author zhoujw
 * @since 2020-04-22
 * @version v1.0
 */
public interface SysUserService extends BaseService<SysUser> {
	
	/**
	 * 自定义分页-用户管理列表
	 */
	Page<SysUserVO> list(Map<String, Object> params);
	
	SysUserVO save(SysUserVO vo);
	
	SysUserVO update(SysUserVO vo);
}
