package com.hxd.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hxd.core.base.BaseServiceImpl;
import com.hxd.entity.SysUser;
import com.hxd.exception.DbException;
import com.hxd.mapper.SysUserMapper;
import com.hxd.pojo.Page;
import com.hxd.pojo.SysRoleUserVO;
import com.hxd.pojo.SysUserVO;
import com.hxd.service.SysRoleUserService;
import com.hxd.service.SysUserService;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;


/**
 * <p>
    * 系统基础 - 用户表 服务实现类
    * </p>
 *
 * @author zhoujw
 * @since 2020-04-22
 * @version v1.0
 */
@Service
public class SysUserServiceImpl extends BaseServiceImpl<SysUserMapper, SysUser> implements SysUserService {

	@Autowired
	private SysRoleUserService sysRoleUserService;
	
	@Override
	public Page<SysUserVO> list(Map<String, Object> params) {
		Page<SysUserVO> page = new Page<SysUserVO>(params);
		page.setRecords(mapper.selectList(params));
		return page;
	}
	
	@Override
	@Transactional
	public SysUserVO save(SysUserVO vo) {
		SysUser entity = BeanUtil.copyProperties(vo, SysUser.class);
		entity.setId(IW.nextId());
		if(!save(entity)) {
			throw new DbException("新增失败！");
		}
		vo.setId(entity.getId());
		return vo;
	}

	@Override
	@Transactional
	public SysUserVO update(SysUserVO vo) {
		SysUser entity = BeanUtil.copyProperties(vo, SysUser.class);
		if(updateById(entity)) {
			if(ObjectUtil.isNotNull(vo.getRoleId())) {
				SysRoleUserVO sysRoleUserVO = new SysRoleUserVO();
				sysRoleUserVO.setUserId(vo.getId());
				sysRoleUserVO.setRoleId(vo.getRoleId());
				if(sysRoleUserService.saveOrUpdate(sysRoleUserVO)) {
					throw new DbException("安全设置失败！");
				}
			}
		}else {
			throw new DbException("修改失败！");
		}
		return vo;
	}
}
