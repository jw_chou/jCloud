package com.hxd.service;

import com.hxd.core.base.BaseService;
import com.hxd.entity.SysRoleUser;
import com.hxd.pojo.SysRoleUserVO;

/**
 * <p>
    * 系统基础 - 用户角色表 服务类
    * </p>
 *
 * @author zhoujw
 * @since 2020-04-28
 * @version v1.0
 */
public interface SysRoleUserService extends BaseService<SysRoleUser> {
	
	/**
	 * 用户角色新增修改
	 */
	Boolean saveOrUpdate(SysRoleUserVO vo);
}
