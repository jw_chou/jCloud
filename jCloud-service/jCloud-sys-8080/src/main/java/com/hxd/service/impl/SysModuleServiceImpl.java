package com.hxd.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.hxd.core.base.BaseServiceImpl;
import com.hxd.entity.SysModule;
import com.hxd.mapper.SysModuleMapper;
import com.hxd.pojo.SysModuleVO;
import com.hxd.service.SysModuleService;

/**
 * <p>
    * 系统基础 - 模块信息 服务实现类
    * </p>
 *
 * @author zhoujw
 * @since 2020-04-22
 * @version v1.0
 */
@Service
public class SysModuleServiceImpl extends BaseServiceImpl<SysModuleMapper, SysModule> implements SysModuleService {
	
	@Override
	public List<SysModuleVO> getTree() {
		return mapper.selectList();
	}
}
