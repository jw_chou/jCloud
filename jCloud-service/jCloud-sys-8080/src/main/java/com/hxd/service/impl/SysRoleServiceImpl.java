package com.hxd.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hxd.core.base.BaseServiceImpl;
import com.hxd.entity.SysRole;
import com.hxd.exception.DbException;
import com.hxd.mapper.SysRoleMapper;
import com.hxd.pojo.SysRoleVO;
import com.hxd.service.SysRoleService;

import cn.hutool.core.bean.BeanUtil;

/**
 * <p>
    * 系统基础 - 角色表 服务实现类
    * </p>
 *
 * @author zhoujw
 * @since 2020-04-23
 * @version v1.0
 */
@Service
public class SysRoleServiceImpl extends BaseServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {

	@Override
	@Transactional
	public SysRoleVO save(SysRoleVO vo) {
		SysRole entity = BeanUtil.copyProperties(vo, SysRole.class);
		entity.setId(IW.nextId());
		if(!save(entity)) {
			throw new DbException("新增失败！");
		}
		vo.setId(entity.getId());
		return vo;
	}

	@Override
	@Transactional
	public SysRoleVO update(SysRoleVO vo) {
		SysRole entity = BeanUtil.copyProperties(vo, SysRole.class);
		if(!updateById(entity)) {
			throw new DbException("修改失败！");
		}
		return vo;
	}
}
