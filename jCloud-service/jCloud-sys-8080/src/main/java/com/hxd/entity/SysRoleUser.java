package com.hxd.entity;

import com.hxd.core.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;

/**
 * <p>
 * 系统基础 - 用户角色表
 * </p>
 *
 * @author zhoujw
 * @since 2020-04-28
 */
@Data
@FieldNameConstants
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class SysRoleUser extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 角色id
     */
    private Long roleId;

    /**
     * 用户id
     */
    private Long userId;


}
