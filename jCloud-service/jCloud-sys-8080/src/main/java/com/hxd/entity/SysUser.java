package com.hxd.entity;

import com.hxd.core.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 系统基础 - 用户表
 * </p>
 *
 * @author zhoujw
 * @since 2020-04-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class SysUser extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 用户编码
     */
    private String code;

    /**
     * 用户昵称
     */
    private String name;

    /**
     * 用户类型（0：普通用户；1：超级管理员；）
     */
    private Integer type;

    /**
     * 用户状态（0：正常；1：停用；）
     */
    private Integer state;

    /**
     * 所属组织id
     */
    private Long orgId;

    /**
     * 所属组织名称
     */
    private String orgName;

    /**
     * 性别
     */
    private Integer gender;

    /**
     * 身份号码
     */
    private String idCard;

    /**
     * 出生日期
     */
    private Long birthDate;

    /**
     * 职位信息
     */
    private String positions;

    /**
     * 是否领导
     */
    private Integer isLeader;

    /**
     * 移动电话
     */
    private String phone;

    /**
     * 座机号码
     */
    private String telephone;

    /**
     * 电子邮箱
     */
    private String email;

    /**
     * 家庭住址
     */
    private String address;

    /**
     * 个人头像
     */
    private String headImage;

    /**
     * 用户签名
     */
    private String signImage;

    /**
     * 个性配置（JSON）
     */
    private String configure;

    /**
     * 设备类型
     */
    private String devicePlatform;

    /**
     * 设备号
     */
    private String deviceUid;

    /**
     * 排序编码
     */
    private Integer sort;

    /**
     * 备注说明
     */
    private String remark;

    /**
     * 是否删除（0：否；1：是；）
     */
    private Integer isDel;

    /**
     * 创建时间
     */
    private Long createDate;

    /**
     * 创建人
     */
    private Long createUserId;

    /**
     * 创建人名称
     */
    private String createUserName;

    /**
     * 更新时间
     */
    private Long updateDate;

    /**
     * 更新人
     */
    private Long updateUserId;

    /**
     * 更新人名称
     */
    private String updateUserName;


}
