package com.hxd.entity;

import com.hxd.core.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 系统基础 - 角色表
 * </p>
 *
 * @author zhoujw
 * @since 2020-04-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class SysRole extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 角色类型（0：普通角色；1：系统设定）
     */
    private Integer category;

    /**
     * 角色编码
     */
    private String code;

    /**
     * 角色名称
     */
    private String name;

    /**
     * 排序编码
     */
    private Integer sort;

    /**
     * 备注说明
     */
    private String remark;

    /**
     * 是否删除（0：否；1：是）
     */
    private Integer isDel;

    /**
     * 创建时间
     */
    private Long createDate;

    /**
     * 创建人
     */
    private Long createUserId;

    /**
     * 创建人名称
     */
    private String createUserName;

    /**
     * 更新时间
     */
    private Long updateDate;

    /**
     * 更新人
     */
    private Long updateUserId;

    /**
     * 更新人名称
     */
    private String updateUserName;


}
