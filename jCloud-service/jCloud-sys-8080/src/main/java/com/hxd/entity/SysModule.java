package com.hxd.entity;

import com.hxd.core.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 系统基础 - 模块信息
 * </p>
 *
 * @author zhoujw
 * @since 2020-04-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class SysModule extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 模块编码
     */
    private String code;

    /**
     * 模块名称
     */
    private String name;

    /**
     * 模块状态（0：正常；1：停用）
     */
    private Integer state;

    /**
     * 上级id
     */
    private Long parentId;

    /**
     * 权限控制（0：否；1：是）
     */
    private Integer isRole;

    /**
     * 链接地址
     */
    private String link;

    /**
     * 显示图标
     */
    private String icon;

    /**
     * 排序编码
     */
    private Integer sort;

    /**
     * 备注说明
     */
    private String remark;


}
