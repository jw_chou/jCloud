package ${package.Service};

import com.hxd.Result;
import com.hxd.core.base.BaseService;
import com.hxd.entity.${entity};
import com.hxd.pojo.${entity}VO;

/**
 * <p>
    * ${table.comment!} 服务类
    * </p>
 *
 * @author ${author}
 * @since ${date}
 * @version v1.0
 */
public interface ${table.serviceName} extends BaseService<${entity}> {
	
	Result save(${entity}VO vo);
	
	Result update(${entity}VO vo);
}
