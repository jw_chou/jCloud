package ${package.ServiceImpl};

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hxd.Result;
import com.hxd.core.base.BaseServiceImpl;
import com.hxd.entity.${entity};
import ${package.Mapper}.${table.mapperName};
import com.hxd.pojo.${entity}VO;
import ${package.Service}.${table.serviceName};

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;

/**
 * <p>
    * ${table.comment!} 服务实现类
    * </p>
 *
 * @author ${author}
 * @since ${date}
 * @version v1.0
 */
@Service
public class ${table.serviceImplName} extends BaseServiceImpl<${table.mapperName}, ${entity}> implements ${table.serviceName} {

	@Override
	@Transactional
	public Result save(${entity}VO vo) {
		${entity} entity = BeanUtil.copyProperties(vo, ${entity}.class);
		entity.setId(IW.nextId());
		if(!save(entity)) {
			return Result.failure("新增失败！");
		}
		return Result.success(entity.getId());
	}

	@Override
	@Transactional
	public Result update(${entity}VO vo) {
		${entity} entity = getById(vo.getId());
		if(ObjectUtil.isNull(entity)) {
			return Result.failure("未查询到该条记录，修改失败！");
		}
		BeanUtil.copyProperties(vo, entity);
		if(!updateById(entity)) {
			return Result.failure("修改失败！");
		}
		return Result.success(entity.getId());
	}
}
