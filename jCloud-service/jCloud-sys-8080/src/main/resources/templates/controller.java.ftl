package ${package.Controller};

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hxd.Result;
import com.hxd.core.base.BaseController;
import com.hxd.entity.${entity};
import com.hxd.pojo.${entity}VO;
import com.hxd.service.${entity}Service;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * <p>
    * ${table.comment!} 前端控制器
    * </p>
 *
 * @author ${author}
 * @since ${date}
 * @version v1.0
 */
@Api(tags="${table.comment!}")
<#if restControllerStyle>
@RestController
<#else>
@Controller
</#if>
@RequestMapping("<#if package.ModuleName??>/${package.ModuleName}</#if>/<#if controllerMappingHyphenStyle??>${controllerMappingHyphen}<#else>${table.entityPath}</#if>")
<#if superControllerClass??>
public class ${table.controllerName} extends BaseController<${entity}Service, ${entity}> {
<#else>
public class ${table.controllerName} {
</#if>

    @PostMapping
    @ApiOperation(value = "新增数据")
    public Result save(@RequestBody ${entity}VO vo) {
		return service.save(vo);
    }
    
    @PutMapping
    @ApiOperation(value = "更新数据")
    public Result update(@RequestBody ${entity}VO vo) {
		return service.update(vo);
    }
}
