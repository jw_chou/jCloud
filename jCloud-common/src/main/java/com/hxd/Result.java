package com.hxd;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import cn.hutool.http.HttpStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@ApiModel(value = "返回结果")
@JsonInclude(Include.NON_NULL)
@EqualsAndHashCode(callSuper=false)
public class Result<T> implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "返回状态码")
    private int code;
	
	@ApiModelProperty(value = "返回信息")
    private String msg;
	
	@ApiModelProperty(value = "返回数据")
    private T data;

    public Result() {
    }

    public Result(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Result(int code, String msg, T data) {
        this(code, msg);
        this.data = data;
    }

    public boolean isSuccess() {
        return this.code == 200;
    }

    /**
     * 返回
     *
     * @param success
     * @return
     */
    public static <T> Result<T> render(boolean success) {
        return render(success, "OK", "FAIL");
    }

    /**
     * 返回
     *
     * @param success
     * @param failureMsg
     * @return
     */
    public static <T> Result<T> render(boolean success, String failureMsg) {
        return render(success, "OK", failureMsg);
    }


    /**
     * 返回
     *
     * @param success
     * @param successMsg
     * @param failureMsg
     * @return
     */
    public static <T> Result<T> render(boolean success, String successMsg, String failureMsg) {
        return success ? new Result<T>(HttpStatus.HTTP_OK, successMsg) : new Result<T>(HttpStatus.HTTP_BAD_REQUEST, failureMsg);
    }

    /**
     * 请求成功
     *
     * @return
     */
    public static <T> Result<T> success() {
        return new Result<T>(HttpStatus.HTTP_OK, "OK");
    }

    /**
     * 请求成功
     *
     * @param obj
     * @return
     */
    public static <T> Result<T> success(T obj) {
        return new Result<T>(HttpStatus.HTTP_OK, "OK", obj);
    }

    /**
     * 请求成功
     *
     * @param msg
     * @param obj
     * @return
     */
    public static <T> Result<T> success(String msg, T obj) {
        return new Result<T>(HttpStatus.HTTP_OK, msg, obj);
    }

    /**
     * 请求失败
     *
     * @param message
     * @return
     */
    public static <T> Result<T> failure(String message) {
        return new Result<T>(HttpStatus.HTTP_BAD_REQUEST, message);
    }

    /**
     * 请求失败
     *
     * @param code
     * @param message
     * @return
     */
    public static <T> Result<T> failure(Integer code, String message) {
        return new Result<T>(code, message);
    }

    /**
     * 未登录401
     */
    public static <T> Result<T> unauthorized() {
        return new Result<T>(HttpStatus.HTTP_UNAUTHORIZED, "未登录");
    }

    /**
     * 未登录401
     */
    public static <T> Result<T> unauthorized(String message) {
        return new Result<T>(HttpStatus.HTTP_UNAUTHORIZED, message);
    }

    /**
     * 未授权403
     */
    public static <T> Result<T> forbidden() {
        return new Result<T>(HttpStatus.HTTP_FORBIDDEN, "未授权");
    }

    /**
     * 未授权403
     */
    public static <T> Result<T> forbidden(String message) {
        return new Result<T>(HttpStatus.HTTP_FORBIDDEN, message);
    }

    /**
     * 没有找到404
     */
    public static <T> Result<T> notFound() {
        return new Result<T>(HttpStatus.HTTP_NOT_FOUND, "没有找到资源");
    }

    /**
     * 没有找到404
     */
    public static <T> Result<T> notFound(String message) {
        return new Result<T>(HttpStatus.HTTP_NOT_FOUND, message);
    }

    /**
     * 服务器错误
     *
     * @return
     */
    public static <T> Result<T> error() {
        return new Result<T>(HttpStatus.HTTP_INTERNAL_ERROR, "服务器错误");
    }

    /**
     * 服务器错误
     *
     * @param message
     * @return
     */
    public static <T> Result<T> error(String message) {
        return new Result<T>(HttpStatus.HTTP_INTERNAL_ERROR, message);
    }

    public boolean hasException() {
        if (this.code != HttpStatus.HTTP_OK) {
            return true;
        }
        return false;
    }

}
