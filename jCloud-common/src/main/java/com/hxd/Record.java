package com.hxd;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;

/**
 * @author Yan
 */
@SuppressWarnings({"serial", "rawtypes", "unchecked"})
public class Record extends HashMap<String, Object> {

    public Record() {
    }

    public static Record create(Map<String, Object> params) {
        return new Record().setColumns(params);
    }

    public Record setColumns(Map<String, Object> map) {
        super.putAll(map);
        return this;
    }

    public Record setColumns(Record record) {
        super.putAll(record);
        return this;
    }

    public Record remove(String key) {
        super.remove(key);
        return this;
    }

    public Record remove(String... columns) {
        if (columns != null) {
            String[] var2 = columns;
            int var3 = columns.length;

            for (int var4 = 0; var4 < var3; ++var4) {
                String c = var2[var4];
                super.remove(c);
            }
        }

        return this;
    }

    public Record removeNullValueColumns() {
        Iterator it = super.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Object> e = (Map.Entry) it.next();
            if (e.getValue() == null) {
                it.remove();
            }
        }
        return this;
    }

    public Record keep(String... columns) {
        if (columns != null && columns.length > 0) {
            Map<String, Object> newColumns = new HashMap(columns.length);
            String[] var3 = columns;
            int var4 = columns.length;
            for (int var5 = 0; var5 < var4; ++var5) {
                String c = var3[var5];
                if (super.containsKey(c)) {
                    newColumns.put(c, super.get(c));
                }
            }
            super.clear();
            super.putAll(newColumns);
        } else {
            super.clear();
        }

        return this;
    }

    public Record keep(String column) {
        if (super.containsKey(column)) {
            Object keepIt = super.get(column);
            super.clear();
            super.put(column, keepIt);
        } else {
            super.clear();
        }
        return this;
    }

    public Record set(String column, Object value) {
        super.put(column, value);
        return this;
    }

    public Record setIfNotBlank(String key, String value) {
        if (StrUtil.isNotBlank(value)) {
            set(key, value);
        }
        return this;
    }

    public Record setIfNotNull(String key, Object value) {
        if (value != null) {
            set(key, value);
        }
        return this;
    }

    public Object get(String column) {
        return super.get(column);
    }

    public Object get(String column, Object defaultValue) {
        Object result = super.get(column);
        return result != null ? result : defaultValue;
    }

    public <T> T getAs(Object key) {
        return (T) get(key);
    }

    public Object getObject(String column) {
        return super.get(column);
    }

    public Object getObject(String column, Object defaultValue) {
        Object result = super.get(column);
        return result != null ? result : defaultValue;
    }

    public String getStr(String column) {
        return this.getStr(column, null);
    }

    public String getStr(String column, String defaultValue) {
        return Convert.toStr(super.get(column), defaultValue);
    }

    public Integer getInt(String column) {
        return this.getInt(column, null);
    }

    public Integer getInt(String column, Integer defaultValue) {
        return Convert.toInt(super.get(column), defaultValue);
    }

    public Long getLong(String column) {
        return this.getLong(column, null);
    }

    public Long getLong(String column, Long defaultValue) {
        return Convert.toLong(super.get(column), defaultValue);
    }

    public BigInteger getBigInteger(String column) {
        return this.getBigInteger(column);
    }

    public BigInteger getBigInteger(String column, BigInteger defaultValue) {
        return Convert.toBigInteger(super.get(column), defaultValue);
    }

    public Date getDate(String column) {
        return this.getDate(column, null);
    }

    public Date getDate(String column, Date defaultValue) {
        return Convert.toDate(super.get(column), defaultValue);
    }

    public Double getDouble(String column) {
        return this.getDouble(column, null);
    }

    public Double getDouble(String column, Double defaultValue) {
        return Convert.toDouble(super.get(column), defaultValue);
    }

    public Float getFloat(String column) {
        return this.getFloat(column, null);
    }

    public Float getFloat(String column, Float defaultValue) {
        return Convert.toFloat(super.get(column), defaultValue);
    }

    public Short getShort(String column) {
        return this.getShort(column, null);
    }

    public Short getShort(String column, Short defaultValue) {
        return Convert.toShort(super.get(column), defaultValue);
    }

    public Byte getByte(String column) {
        return this.getByte(column, null);
    }

    public Byte getByte(String column, Byte defaultValue) {
        return Convert.toByte(super.get(column), defaultValue);
    }

    public Boolean getBoolean(String column) {
        return this.getBoolean(column, false);
    }

    public Boolean getBoolean(String column, Boolean defaultValue) {
        return Convert.toBool(super.get(column), defaultValue);
    }

    public BigDecimal getBigDecimal(String column) {
        return this.getBigDecimal(column, null);
    }

    public BigDecimal getBigDecimal(String column, BigDecimal defaultValue) {
        return Convert.toBigDecimal(super.get(column), defaultValue);
    }

    public Number getNumber(String column) {
        return this.getNumber(column, null);
    }

    public Number getNumber(String column, Number defaultValue) {
        return Convert.toNumber(super.get(column), defaultValue);
    }

    public byte[] getBytes(String column) {
        return (byte[]) ((byte[]) super.get(column));
    }

    public String[] getColumnNames() {
        Set<String> attrNameSet = super.keySet();
        return (String[]) attrNameSet.toArray(new String[attrNameSet.size()]);
    }

    public Object[] getColumnValues() {
        Collection<Object> attrValueCollection = super.values();
        return attrValueCollection.toArray(new Object[attrValueCollection.size()]);
    }

    public boolean isExist(String column) {
        return super.containsKey(column);
    }

    public boolean isNotBlank(String column) {
        return this.get(column) != null;
    }

    public String toJson() {
        return JSON.toJSONString(this);
    }
}
