package com.hxd.utils;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.yaml.snakeyaml.Yaml;

public class YmlUtils {

	private static Map<String, String> result = new HashMap<>();

	@SuppressWarnings("unchecked")
	public static Map<String,String> getYmlByFileName() {
		result = new HashMap<>();
		InputStream in = YmlUtils.class.getClassLoader().getResourceAsStream("bootstrap.yml");
		Yaml props = new Yaml();
	    Object obj = props.loadAs(in,Map.class);
	    Map<String, Object> param = (Map<String, Object>) obj;
	    for(Map.Entry<String, Object> entry : param.entrySet()) {
	    	String key = entry.getKey();
	    	Object val = entry.getValue();
	    	if(val instanceof Map) {
	    		forEachYaml(key,(Map<String, Object>) val);
	    	} else {
	    		result.put(key,val.toString());
	    	}
	    }
	    return result;
	}
	
	/**
	 * 根据key获取值
	 */
	public static String getConfig(String key) {
		Map<String, String> map = getYmlByFileName();
	    if(null != map)
	    	return map.get(key);
	    return null;
	}
	
	/**
	 * 遍历yml文件，获取map集合
	 */
	@SuppressWarnings("unchecked")
	public static Map<String, String> forEachYaml(String key_str, Map<String, Object> obj) {
		for(Map.Entry<String, Object> entry : obj.entrySet()){
			String key = entry.getKey();
			Object val = entry.getValue();
			String str_new = "";
			if(StringUtils.isNotNull(key_str)){
				str_new = key_str+ "." + key;
			} else {
				str_new = key;
			}
			if(val instanceof Map) {
				forEachYaml(str_new, (Map<String, Object>) val);
			} else {
				if(null != val)
					result.put(str_new, val.toString());
			}
		}
	    return result;
	}
	  
}
