package com.hxd.utils;

public class StringUtils extends org.apache.commons.lang3.StringUtils {

	public static boolean isNull(Object object) {
        return object == null;
    }
	
	public static boolean isNotNull(Object object) {
        return object != null;
    }
	
	/**
     * 获取参数不为空值
     * 
     * @param value defaultValue 要判断的value
     * @return value 返回值
     */
    public static <T> T nvl(T value, T defaultValue) {
        return value != null ? value : defaultValue;
    }
    
    public static String replacePlaceholder(String str, String ... val) {
		for (int i = 0; i < val.length; i++) {
			str = str.replace("{"+i+"}", val[i]);
		}
		return str;
	}
	
}
