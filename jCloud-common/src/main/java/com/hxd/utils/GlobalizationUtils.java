package com.hxd.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Properties;

/**
 * properties文件获取工具类
 */
public class GlobalizationUtils {
	
    private static final Logger logger = LoggerFactory.getLogger(GlobalizationUtils.class);
    
    private static Properties cnprops;
    private static Properties enprops;
    
    static{
        loadCnProps();
        loadEnProps();
    }

    synchronized static private void loadCnProps(){
    	cnprops = new Properties();
        InputStream in = null;
        try {
            in = GlobalizationUtils.class.getClassLoader().getResourceAsStream("i18n/zh_cn.properties");
            cnprops.load(in);
        } catch (FileNotFoundException e) {
            logger.error("zh_cn.properties文件未找到");
        } catch (IOException e) {
            logger.error(e.getMessage());
        } finally {
            try {
                if(null != in) {
                    in.close();
                }
            } catch (IOException e) {
            	logger.error(e.getMessage());
            }
        }
    }

    public static String cn(String key) {
    	return cn(key, new String());
    }

    public static String cn(String key, String defaultValue) {
        if(null == cnprops) {
        	loadCnProps();
        }
        try {
			return new String((cnprops.getProperty(key, defaultValue)).getBytes("ISO-8859-1"), "utf-8");
		} catch (UnsupportedEncodingException e) {
			
		}
		return defaultValue;
    }
    
    synchronized static private void loadEnProps(){
    	enprops = new Properties();
        InputStream in = null;
        try {
            in = GlobalizationUtils.class.getClassLoader().getResourceAsStream("i18n/en_us.properties");
            enprops.load(in);
        } catch (FileNotFoundException e) {
            logger.error("en_us.properties文件未找到");
        } catch (IOException e) {
        	logger.error(e.getMessage());
        } finally {
            try {
                if(null != in) {
                    in.close();
                }
            } catch (IOException e) {
            	logger.error(e.getMessage());
            }
        }
    }

    public static String en(String key) {
        return en(key, new String());
    }

    public static String en(String key, String defaultValue) {
        if(null == enprops) {
        	loadCnProps();
        }
        return enprops.getProperty(key, defaultValue);
    }
    
}
