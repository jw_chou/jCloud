package com.hxd.exception.enums;

import lombok.Getter;

/**
 * 用户异常
 */
@Getter
public enum ExceptionEnum {

	USER_NOLOGIN(401, "当前登录已失效，请重新登录", "User is not logged in"),
	USER_UNAUTH(403, "账号未授权，请联系管理员", "User is not authorized"),
	DATA_ERROR(404, "数据错误", "Data error"),
	DB_ERROR(500, "服务器异常，请稍后再试", "Operation exception");
	
	public Integer code;
	public String cnMsg;
	public String enMsg;
	
	ExceptionEnum(Integer code, String cnMsg, String enMsg){
		this.code = code;
		this.cnMsg = cnMsg;
		this.enMsg = enMsg;
	}
}
