package com.hxd.exception;

import com.hxd.exception.base.BaseException;
import com.hxd.exception.enums.ExceptionEnum;

/**
 * 操作异常
 */
public class DbException extends BaseException {

	private static final long serialVersionUID = 1L;
	
	public DbException() {
		super(ExceptionEnum.DB_ERROR);
	}
	
	public DbException(String msg) {
		super(ExceptionEnum.DB_ERROR.getCode(), msg);
	}

}
