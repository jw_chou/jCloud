package com.hxd.exception;

import com.hxd.exception.base.BaseException;
import com.hxd.exception.enums.ExceptionEnum;

/**
 * 数据异常-数据缺少或不符合预期
 */
public class DataException extends BaseException {

	private static final long serialVersionUID = 1L;
	
	public DataException() {
		super(ExceptionEnum.DATA_ERROR);
	}
	
	public DataException(String msg) {
		super(ExceptionEnum.DATA_ERROR.getCode(), msg);
	}
	
}
