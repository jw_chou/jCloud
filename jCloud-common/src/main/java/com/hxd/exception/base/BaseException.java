package com.hxd.exception.base;

import com.hxd.exception.enums.ExceptionEnum;

import lombok.Getter;

@Getter
public class BaseException extends RuntimeException {

	private static final long serialVersionUID = 1L;

    /**
     * 错误码
     */
    private Integer code;

    /**
     * 错误消息
     */
    private String msggess;
    
    public BaseException(ExceptionEnum exceptionEnum) {
    	this.code = exceptionEnum.code;
    	// 根据请求参数或配置返回语言
    	this.msggess = exceptionEnum.cnMsg;
    }
    
    public BaseException(Integer code, String msggess) {
        this.code = code;
        this.msggess = msggess;
    }
    
}
