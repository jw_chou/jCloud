package com.hxd.constant;

public class Constants {

	/**
     * UTF-8 字符集
     */
    public static final String UTF8 = "UTF-8";

    /**
     * 通用成功标识
     */
    public static final String SUCCESS = "0";

    /**
     * 通用失败标识
     */
    public static final String FAIL = "1";
    
    /**
     * 配置文件-关闭
     */
    public static final String CLOSE = "0";
    
    /**
     * 配置文件-开启
     */
    public static final String OPEN = "1";
    
    /**
     * 状态-正常
     */
    public static final Integer NORMAL = 0;
    
    /**
     * 状态-禁用
     */
    public static final Integer DISABLE = 1;
    
    /**
     * 当前记录起始索引
     */
    public static final String PAGE_NUM = "pageNum";

    /**
     * 每页显示记录数
     */
    public static final String PAGE_SIZE = "pageSize";
    
    /**
     * token
     */
    public static final String TOKEN = "token";
    
    /**
     * 权限开关常量
     */
    public static final String AUTH_SWITCH = "auth.switch";
    
}
