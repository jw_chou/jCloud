package com.hxd.constant;

public class FeignClientConstants {

	/**
     * 业务
     */
    public static final String API = "api";
    
    /**
     * 系统
     */
    public static final String SYSTEM = "sys";
    
    /**
     * 授权
     */
    public static final String AUTH = "auth";
    
}
